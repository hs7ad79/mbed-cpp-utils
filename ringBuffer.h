/**
 * @file ringBuffer.h
 * @author 	Hugo S (hugoschaaf98@domain.com)
 * @brief 	A simple ring buffer implementation which doesn't use
 * 			dynamic memory allocation.
 * 
 * @warning RingBuffer class is implemented as a template so you can use it 
 * 			with types of your choice. In some cases it perfoms copies of
 * 			the stored object. Keep in mind that these copies might be resource costly.
 * 
 * @version 0.3
 * @date 2021-05-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include <cstddef>

namespace util
{
    template <typename T, std::size_t N>
    class RingBuffer
    {
    public:
        RingBuffer() : count_{0}, head_{0}, tail_{0}
        {
        }
        RingBuffer(T &val) : RingBuffer()
        {
            fill(std::move(val));
        }

        bool
        push(T &&val)
        {
            if (isFull())
                return false; // First we check if fifo is not already full

            data_[head_] = std::move(val);
            head_        = (nextIndex_(head_) == tail_) ? head_ : nextIndex_(head_); // we shouldnt override tail_
            count_++;
            return true;
        }

        bool
        push(const T &val)
        {
            auto tmp = val;
            return push(std::move(tmp));
        }

        T
        pop()
        {
            if (available() > 0)
            {
                auto val = std::move(data_[tail_]);
                tail_    = nextIndex_(tail_);
                count_--;
                return val;
            }
            else
                return T{};
        }

        const T &
        peek() { return data_[tail_]; }

        void
        fill(T &&val)
        {
            for (auto &e : data_)
                e = val;
            clear();
        }

        void
        fill(const T &val)
        {
            auto tmp = val;
            fill(std::move(tmp));
        }

        void
        clear()
        {
            head_  = 0;
            tail_  = 0;
            count_ = 0;
        }

        std::size_t
        available() { return count_; }

        bool
        isFull() { return count_ == N; }

        constexpr std::size_t
        size() { return N; }

    private:
        T           data_[N]; // The buffer actually containing data
        std::size_t count_;   // Count of unread elements in the buffer
        std::size_t head_;    // Index of the last written value
        std::size_t tail_;    // Index of the firts unread value

        // Compute the next index from the given index
        std::size_t
        nextIndex_(std::size_t index)
        {
            return (index + 1) % N;
        }
    };

    template <typename T, std::size_t N>
    inline std::size_t flatten(RingBuffer<T, N> &ring_buffer, T *flat_buffer, std::size_t size)
    {
        std::size_t i     = 0;
        auto        count = std::min(ring_buffer.available(), size);
        for (; i < count; ++i)
        {
            flat_buffer[i] = ring_buffer.pop();
        }
        return i;
    }

    template <typename T, std::size_t N, std::size_t M>
    inline std::size_t flatten(RingBuffer<T, N> &ring_buffer, T (&flat_buffer)[M])
    {
        return flatten(ring_buffer, flat_buffer, M);
    }

    template <typename T, std::size_t N>
    inline std::size_t append(RingBuffer<T, N> &ring_buffer, T *data, std::size_t size)
    {
        std::size_t count = 0;
        while (count < size && ring_buffer.push(data[count]))
            count++;
        return count;
    }

    template <typename T, std::size_t N, std::size_t M>
    inline std::size_t append(RingBuffer<T, N> &ring_buffer, T (&data)[M])
    {
        return append(ring_buffer, data, M);
    }

} // namespace util

#endif // RINGBUFFER_H_