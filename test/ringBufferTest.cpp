/**
 * @file ringBufferTest.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @note compile with <g++ -std=c++17 -I../ -Og -g3 -Wall -Wextra -pedantic -o ringBufferTest.out ringBufferTest.cpp>
 * @version 0.1
 * @date 2021-05-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <cstdint>
#include <string>

#include "noisy.h"
#include "ringBuffer.h"

void testNoisyBuffer()
{
	std::cout<<"---------- "<<__func__<<"() ----------\n\n";

	using NoisyStr = util::Noisy<std::string>;

    util::RingBuffer<NoisyStr, 2> fifo;
	std::cout<<"\n-- Filling buffer --\n";
	for(char c = 'a'; not fifo.isFull(); c++) { fifo.push(NoisyStr{std::string{c}}); }

	std::cout<<"\n-- Printing buffer --\n";
	for(std::size_t i=0; fifo.available(); i++)
	{
		std::size_t elem = fifo.available();
		std::cout<<"There are <"<<elem<<"> available. The next is : "<<fifo.pop()<<'\n';
	}

	fifo.clear();

	// Trigger a push(const T&)
	std::cout<<"Creating <a = NoisyStr{\"hello from a\"}> \n";
	auto a = NoisyStr{"hello from a"};
	std::cout<<"Pushing <a> into fifo\n";
	fifo.push(a);
	std::cout<<"<a> is still <"<<a<<">\n";

	// Trigger a push(T&&)
	std::cout<<"Creating <b = NoisyStr{\"hello from b\"}> \n";
	auto b = NoisyStr{"hello from b"};
	std::cout<<"Pushing <b> into fifo\n";
	fifo.push(std::move(b));
	std::cout<<"<b> is now <"<<b<<">\n";
}


int 
main()
{
	std::cout<<"---------- Start ----------\n\n";
	
	testNoisyBuffer();

    std::cout<<"\n---------- End ----------\n\n";
    
	return 0;
}